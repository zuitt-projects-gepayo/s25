/*

	Activity: (database: session25)

	//Aggregate to count the total number of items supplied by Red Farms ($count stage)


	//Aggregate to count the total number of items with price greater than 50. ($count stage)

	//Aggregate to get the average price of all fruits that are onSale per supplier.($group)

	//Aggregate to get the highest price of fruits that are onSale per supplier. ($group)

	//Aggregate to get the lowest price of fruits that are onSale per supplier. ($group)


	//save your query/commands in the activity.js

*/

//Aggregate to count the total number of items supplied by Red Farms ($count stage)

db.fruits.aggregate([
    {
        $match: {"supplier" : "Red Farm Inc."}
    },
    {
        $count: "totalNumberOfItems"
	}
])


//Aggregate to count the total number of items with price greater than 50. ($count stage)

db.fruits.aggregate([
    {
        $match: {"price" : {$gte: 50}}
    },
    {
        $count: "totalNumberWithPriceGreaterThan50"
	}
])


//Aggregate to get the average price of all fruits that are onSale per supplier.($group)

db.fruits.aggregate([
    {
        $match: {"onSale" : true}
    },
    {
		$group: {_id : "$supplier", avgPricePerSupplier: {$avg: "$price"}}
	}
])


//Aggregate to get the highest price of fruits that are onSale per supplier. ($group)

db.fruits.aggregate([
    {
        $match: {"onSale" : true}
    },
    {
		$group: {_id : "$supplier", highestPricePerSupplier: {$max: "$price"}}
	}
])


//Aggregate to get the lowest price of fruits that are onSale per supplier. ($group)

db.fruits.aggregate([
    {
        $match: {"onSale" : true}
    },
    {
		$group: {_id : "$supplier", lowestPricePerSupplier: {$min: "$price"}}
	}
])