//Aggregation in MongoDB

/*
    MINI ACTIVITY

    >>create a new database callsed session 25

    >>in session25, create a 5 new cocuments in a new fruits collection with the following fields and values:

        name: "Apple",
        supplier: "Red Farms Inc.",
        stocks: 20
        price: 40
        onSale: true

        name: "Banana",
        supplier: "Yellow Farms"
        stocks: 15
        price: 20
        onSale: true

        name: : "Kiwi"
        supplier: "Green Farming and Canning"
        stocks: 25
        price: 50
        onSale: true


        name: "Mango",
        supplier: Yello Farms
        stocks: 10
        price: 60
        onSale: true

        name: "Dragon Fruit"
        supplier: "Red Farms Inc."
        stocks 10
        price: 60
        onSale: true
*/




db.fruits.insertMany([
	{
		"name" : "Apple",
		"supplier" : "Red Farm Inc.",
		"stocks": 20,
		"price": 40,
		"onSale": true
	},
    {
		"name" : "Banana",
		"supplier" : "Yellow Farms",
		"stocks": 15,
		"price": 20,
		"onSale": true
	},
    {
		"name" : "Kiwi",
		"supplier" : "Green Farming and Canning",
		"stocks": 25,
		"price": 50,
		"onSale": true
	},
    {
		"name" : "Mango",
		"supplier" : "Yellow Farms",
		"stocks": 10,
		"price": 60,
		"onSale": true
	},
    {
		"name" : "Dragon Fruit",
		"supplier" : "Red Farm Inc.",
		"stocks": 10,
		"price": 60,
		"onSale": true
	},
    
])

// Aggregation in MongoDB
    //This is the act or process of generating manipulated data and perform operations to create filtered results that thelps in data analysis.
    //this helps ion creating reports from analyzing the data provided in our documents
    //This helps in arriving at summarized information NOT previously shown in our documents

    //Aggregation Piplelines
        // Aggregation is done in 2 to 3 steps. Each stage is called a pipeline
        //the first pipeline in our example is with the use of $match
        //$match is used to pass document or get the documents that will pass our condition
        
            //syntax: {$match: field:value} 
        
        // The second pipeline in our example is with the use of $group
        //$group grouped elements/documents together and create an anlaysis of these grouped documents

            //SYNTAX: {$group {_id: <id> , fieldResult:"valueResult"}}
            //_id: this is us associating an id for aggregated result
                //when id is provided with $supplier, we created aggregated results per supplier
                //$field will refer to a field name that is available that being aggregated on

        //$sum operator will get the total of all the values of the given field. this will allows us to get the total of the values of the given field per group. it will non-numerical values

    db.fruits.aggregate([
        {
            $match: {"onSale" : true}
        },
        {
            $group: {_id : "$supplier", totalStocks: {$sum: "$stocks"}} 
        }
    ])

    db.fruits.aggregate([
        {
            $match: {"onSale" : true}
        },
        {
            $group: {_id : "$supplier", totalPrice: {$sum: "$price"}}
        }
    ])

    db.fruits.aggregate([
        {
            $match: {"supplier" : "Red Farm Inc."}
        },
        {
            $group: {_id : "$supplier", totalStocks: {$sum: "$stocks"}}
        }
    ])

// Average price of all items on sale
    db.fruits.aggregate([
        {
            $match: {"onSale" : true}
        },
        {
            $group: {_id : "avgPriceOnSale", avgPrice: {$avg: "$price"}}
        }
    ])

//Max Operator
    //allows us to get the highest value out of all the values of a given field

    db.fruits.aggregate([
        {
            $match: {"onSale" : true}
        },
        {
            $group: {_id : "maxStocksOnSale", maxStocks: {$max: "$stocks"}}
        }
    ])

//Min Operator
    // gets the lowest value in a given field

    db.fruits.aggregate([
        {
            $match: {"onSale" : true}
        },
        {
            $group: {_id : "minStocksOnSale", minStocks: {$min: "$stocks"}}
        }
    ])

//other stages
    //$count - is a stage we can add after a match stage to count all items

    db.fruits.aggregate([
        {
            $match: {"onSale" : true}
        },
        {
            $count: "itemsOnSale"
        }
    ])

    db.fruits.aggregate([
        {
            $match: {"price" : {$lt: 50}}
        },
        {
            $count: "priceLessThan50"
        }
    ])


/*
    Mini-Activity

    >>count the number of the items that are not on sale

*/
    db.fruits.aggregate([
        {
            $match: {"onSale" : false}
        },
        {
            $count: "notOnSale"
        }
    ])

//Count total number of items per group

db.fruits.aggregate([
    {
        $match:{"onSale": true}
    },
    {
        $group: {_id: "$supplier", noOfItems:{$sum:1}}
    }
])

//you can have multiple field results per aggregated result

db.fruits.aggregate([
    {
        $match:{"onSale": true}
    },
    {
        $group: {_id: "$supplier", noOfItems:{$sum:1}, totalStocks: {sum: "$stocks"}}
    }
])

// $out - save/output your aggregated result from $group in a new collection

    db.fruits.aggregate([
        {
            $match: {"onSale" : true}
        },
        {
            $group: {_id : "$supplier", totalStocks: {$sum : "$stocks"}}
        },
        {
            $out: "totalStocksPerSupplier"
        }
    ])
